#include "nqueens.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>

#define TRUE 1
#define FALSE 0

pthread_mutex_t lock;

unsigned long long total = 0;
int numTheads = 3;
int auxDim;
int auxQueens;

void* threadFunction(void** func) {
	unsigned int *tab = (unsigned int*) *func;
	execute(&tab, auxDim, 0, auxQueens);
	return NULL;
}

//unsigned long long value = 0; 
unsigned long long nqueens(int dim, int queens){
	if(dim < queens)
		return 0;

	if(dim <= 0 || queens <= 0)
		return 0;

	if(queens == 1)
		return pow(2,pow(dim,2)) - 1;

	auxDim = dim;
	auxQueens = queens;

	pthread_t *threads = malloc(sizeof(pthread_t) * numTheads); 
	unsigned long long auxTotal;
	//unsigned int *tab = calloc(dim*dim, sizeof(unsigned int));
	unsigned int **tabSet = malloc(sizeof(unsigned int) * numTheads);
	//execute(&tab, dim, 0, queens);
	
	for(int i = 0; i < numTheads; ++i) {
		tabSet[i] = calloc(dim*dim, sizeof(unsigned int));
	}
	
	for(int i = 0; i < numTheads; ++i) {
		pthread_create(&(threads[i]), NULL, (void*)threadFunction, (void*)&tabSet[i]);
	}

	for(int i = 0; i < numTheads; ++i) {
		pthread_join(threads[i], NULL);	
	}

	for(int i = 0; i < numTheads; ++i) {
		free(tabSet[i]);
	}

	//free(tab);
	auxTotal = total/numTheads;
	total = 0;
	auxDim = -1; //apenas pra confirmar que nao vai dar ruim
	auxQueens = -1;

	return auxTotal;
}

// para verificação de problemas na compilção
// int main(){
// 	//printf("%d\n", TRUE) ;
// 	printf("%llu\n", nqueens(-1,0)); //rainhas insuficientes
// 	printf("%llu\n", nqueens(2,1)); //ok
// 	//printf("%llu\n", nqueens(2,2)); //resultado insatisfatório
// 	printf("%llu\n", nqueens(3,2)); 

// 	return 0;
//}

unsigned int check(unsigned int *tab, int dim, int lin, int col){
	// linha e coluna
	for(int i = 0; i < dim; ++i) {
		if(tab[lin*dim + i] == 1)
			return FALSE;
	}

	for(int i = 0; i < dim; ++i) {
		if(tab[i*dim + col] == 1){
			return FALSE;
		}
	}
	// diagonais
	// 2º e 4º quadrantes
	for(int i = lin, j = col; i >= 0 && j >= 0; --i, --j) {
		if(tab[i*dim + j] == 1)
			return FALSE;
	}
	for(int i = lin, j = col; i < dim && j < dim; ++i, ++j) {
		if(tab[i*dim + j] == 1)
			return FALSE;
	}
	// 1º e 3º quadrantes
	for(int i = lin, j = col; i >= 0 && j < dim; --i, ++j) {
		if(tab[i*dim + j] == 1)
			return FALSE;
	}
	for(int i = lin, j = col; i < dim && j >= 0; ++i, --j) {
		if(tab[i*dim + j] == 1)
			return FALSE;
	}

	return TRUE;
}


void execute(unsigned int **tab, int dim, int col, int queens) {
	if(col == dim){
		pthread_mutex_lock(&lock);
		unsigned long long valueThread = calculeResult(*tab, dim*dim);
		pthread_mutex_unlock(&lock);
		
		if(verifyQueens(*tab, dim, queens)){
			pthread_mutex_lock(&lock);
			total += valueThread;
			pthread_mutex_unlock(&lock);
		}
		
		return;
	}
	
	for(int i = 0; i < dim; i++){
		// verifica se é seguro colocar a rainha naquela coluna
		if(check(*tab, dim, i, col)){
			(*tab)[i*dim + col] = 1;
	
			execute(tab, dim, col + 1, queens);
			
			(*tab)[i*dim + col] = 0;
		}
	}
	execute(tab, dim, col + 1, queens);
}

unsigned long long calculeResult(unsigned int *tab, int tam) {
	unsigned long long value = 0;
	for(int i = 0; i < tam; ++i){
		if(tab[i] == 1){
			value += pow(2.0, i);
		}
	}
	return value;
}


void print_(unsigned int *tab, int tam) {
	for(int i = 0; i < tam; ++i){
		printf("%u ", tab[i]);
	}
	printf("\n");
}


int verifyQueens(unsigned int *tab, int dim, int queens) {
	int count = 0;

	for(int i = 0; i < dim*dim; ++i){
		if(tab[i] == 1){
			++count;
		}
	}

	if(count == queens){
		return TRUE;
	}else{
		return FALSE;
	}
}